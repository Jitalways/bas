-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2018 at 06:33 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bas`
--

-- --------------------------------------------------------

--
-- Table structure for table `bas_register`
--

CREATE TABLE IF NOT EXISTS `bas_register` (
  `register_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_type` varchar(20) NOT NULL,
  `card_no` varchar(13) NOT NULL,
  `national` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `name_eng` varchar(255) NOT NULL,
  `name_thai` varchar(255) NOT NULL,
  `date_of_birth` varchar(100) NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `province` varchar(100) NOT NULL,
  `district` varchar(100) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `area_code` varchar(10) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `mail_address` varchar(150) DEFAULT NULL,
  `title_name_contact` varchar(30) NOT NULL,
  `name_contact` varchar(255) NOT NULL,
  `address_contact` text NOT NULL,
  `telephone_contact` varchar(20) DEFAULT NULL,
  `mobile_contact` varchar(10) NOT NULL,
  `relationship` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bas_register_awards`
--

CREATE TABLE IF NOT EXISTS `bas_register_awards` (
  `awards_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) NOT NULL,
  `type_competition` varchar(150) NOT NULL,
  `name_institution` varchar(255) NOT NULL,
  `name_prize` varchar(255) NOT NULL,
  `date_received` varchar(155) NOT NULL,
  PRIMARY KEY (`awards_id`),
  KEY `register_id_idy` (`register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bas_register_education`
--

CREATE TABLE IF NOT EXISTS `bas_register_education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) NOT NULL,
  `institution_junior` varchar(200) NOT NULL,
  `city_junior` varchar(100) NOT NULL,
  `country_junior` varchar(100) NOT NULL,
  `education_system_junior` varchar(100) NOT NULL,
  `year_graduation_junior` varchar(20) NOT NULL,
  `gpa_junior` varchar(10) NOT NULL,
  `type_certificate_junior` varchar(50) NOT NULL,
  `institution_high` varchar(200) NOT NULL,
  `city_high` varchar(100) NOT NULL,
  `country_high` varchar(100) NOT NULL,
  `education_system_high` varchar(100) NOT NULL,
  `type_certificate_high` varchar(50) NOT NULL,
  PRIMARY KEY (`education_id`),
  KEY `register_id_ided` (`register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bas_register_experience`
--

CREATE TABLE IF NOT EXISTS `bas_register_experience` (
  `experience_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) NOT NULL,
  `period_from` varchar(50) NOT NULL,
  `period_to` varchar(50) NOT NULL,
  `school_name` varchar(255) NOT NULL,
  `state_province` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `relate_document` varchar(100) DEFAULT NULL,
  `additional_experiences` text,
  PRIMARY KEY (`experience_id`),
  KEY `register_id_idx` (`register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bas_register_skill_related`
--

CREATE TABLE IF NOT EXISTS `bas_register_skill_related` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `register_id` int(11) NOT NULL,
  `eng_type_test` varchar(100) NOT NULL,
  `test_scores` varchar(10) NOT NULL,
  `eng_date_taken` varchar(50) NOT NULL,
  `statement_purpose` text NOT NULL,
  `statement` varchar(100) NOT NULL,
  `evidence_high_school` varchar(100) NOT NULL,
  `transcripts` varchar(100) NOT NULL,
  `evidence_english` varchar(100) NOT NULL,
  `evidence_showing` varchar(100) DEFAULT NULL,
  `evidence_prize` varchar(100) DEFAULT NULL,
  `photo` varchar(100) NOT NULL,
  `others` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`skill_id`),
  KEY `register_ids` (`register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

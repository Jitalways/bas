<div class="container">
    <div class="signup-content">
        <div class="signup-form" style="margin:0px auto;">
            <?php
            if (isset($error)) {
                echo '<div class="alert alert-danger">เกิดความผิดพลาด! ไม่สามารถบันทึกข้อมูลลงระบบได้</div>';
            }?>
            <form method="POST" data-toggle="validator" action="<?php echo base_url(); ?>first/update" class="register-form" id="register-form1">
                <input type="hidden" name="register_id" value="<?php echo set_value('register_id', ((!empty($info->register_id)) ? $info->register_id : 0)); ?>">
                <h2>Application Form ( Step 1. )</h2>
                <h4>
                    Apply now
                </h4>
                <h5>
                    To Start a new application, complete the form below: Application for British and American
                    Studies (Internation Program) 2019
                </h5>
                <br/>
                <div class="form-row">
                    <div class="form-group">
                        <label for="card_type">ID Card/Passport :</label>
                        <div class="form-select">
                            <select name="card_type" id="card_type">
                                <option value="ID Card">ID Card</option>
                                <option value="Passport" <?php echo set_select('card_type', 'Passport', ((!empty($info->card_type) && $info->card_type == 'Passport') ? true : false)); ?>>Passport</option>
                            </select>
                            <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="card_no">ID Card/Passport :</label>
                        <input type="text" name="card_no" id="card_no" value="<?php echo set_value('card_no', ((!empty($info->card_no)) ? $info->card_no : '')); ?>" required />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="national">Nationality :</label>
                        <div class="form-select">
                            <select name="national" id="national">
                                <option value="">-- select one --</option>
                                <?php
                                foreach($national_list as $nvalue) {
                                    echo '<option value="'.$nvalue.'" '.set_select('national', $nvalue,((!empty($info->national) && $info->national==$nvalue) ? TRUE : FALSE)).'>'.$nvalue.'</option>';
                                }
                                ?>
                            </select>
                            <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                        </div>
                    </div>
                    <div class="form-group"></div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="email" class="radio-label">Email :</label>
                        <input type="email" name="email" id="email"
                               data-bv-identical="true"
                               data-bv-identical-field="confirm_email"
                               data-bv-identical-message="The email and its confirm are not the same"
                               value="<?php echo set_value('email', ((!empty($info->email)) ? $info->email : '')); ?>" required />
                    </div>
                    <div class="form-group">
                        <label for="confirm_email" class="radio-label">Confirm Email :</label>
                        <input type="email" name="confirm_email" id="confirm_email"
                               data-bv-identical="true"
                               data-bv-identical-field="email"
                               data-bv-identical-message="The email and its confirm are not the same"
                               value="<?php echo set_value('email', ((!empty($info->email)) ? $info->email : '')); ?>" required />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="password" class="radio-label">Password :</label>
                        <input type="password" name="password" id="password" data-bv-identical="true"
                               data-bv-identical-field="confirm_password"
                               minlength="8"
                               data-bv-identical-message="The password and its confirm are not the same"
                               value="<?php echo set_value('password', ((!empty($info->password)) ? $info->password : '')); ?>" required />
                    </div>
                    <div class="form-group">
                        <label for="confirm_password" class="radio-label">Confirm Password :</label>
                        <input type="password" name="confirm_password" id="confirm_password" data-bv-identical="true"
                               data-bv-identical-field="password"
                               minlength="8"
                               data-bv-identical-message="The password and its confirm are not the same"
                               value="<?php echo set_value('password', ((!empty($info->password)) ? $info->password : '')); ?>" required />
                    </div>
                </div>
                <div class="form-submit">
                    <input type="submit" value="Submit Form" class="submit btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var cardtype_text = 'The card_no is not valid'
        //var email_text = 'The email is required and cannot be empty'

        $('#register-form1').bootstrapValidator(
            {
                message: 'This value is not valid',
                fields: {
                    // 'card_type':{
                    //     message: 'The card_type is not valid',
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'The card type is required and cannot be empty'
                    //         }
                    //     }
                    // },
                    'card_no': {
                        message: ' ',
                        validators: {
                            callback: {
                                message: cardtype_text,
                                callback: function (value, validator, $field) {
                                    if($('#card_type').val() == 'Passport') {
                                        var cardno = /[a-zA-Z]{2}[0-9]{7}/;
                                        cardtype_text = 'Passport is not yet valid.'
                                        return cardno.test(value) == true
                                    } else {
                                        var cardno = /[0-9]{13}/;
                                        cardtype_text = 'Card number is not yet valid.'
                                        return cardno.test(value) == true
                                    }
                                }
                            },
                        }
                    },
                    'national':{
                        message: 'The card_no is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The national is required and cannot be empty'
                            }
                        }
                    },

                }
            })
    })
</script>
<?php

class First extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register', 'model');
    }

    public function index()
    {
        $this->update();
    }

    public function update()
    {
       $info = NULL;
       $param = NULL;
        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
            //get info
            $info =$this->model->getRegister($this->uid);
        }

        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            unset($param['confirm_password']);
            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'));
            if($update_id){
                if(empty($info)){
                    $this->_setCookie(array('member'=>$update_id));
                }
                redirect('second');
            }else{
                $this->data['error'] = TRUE;
            }
        }
        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['national_list'] = json_decode(file_get_contents(base_url() . 'themes/default/config/nationalities.json'));
        $this->view('first');
    }

}

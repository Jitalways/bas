<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="txttitle">Panel <?php echo $node_number;?></span>
            <button  class="close removeButton first">
                <i class="glyphicon glyphicon-remove"></i>
            </button>
        </div>
        <div class="panel-body">
            <div class="form-row">
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="type_competition" class="radio-label">Type of competition :</label>
                    <div class="form-select">
                        <select name="type_competition[]" id="type_competition" style="padding:6px 20px; ">
                            <option value="">-- select one --</option>
                            <option value="English language competition">English language competition</option>
                            <option value="debate">debate</option>
                            <option value="speech">speech</option>
                            <option value="General knowledge quiz">General knowledge quiz</option>
                        </select>
                        <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="name_institution" class="radio-label">Name of host institution/organization :</label>
                    <input type="text" name="name_institution[]" style="padding:6px 20px; " id="name_institution"  />
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="name_prize" class="radio-label">Name of the prize/award :</label>
                    <input type="text" name="name_prize[]" style="padding:6px 20px; " id="name_prize"  />
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label for="date_received" class="radio-label">Date received:</label>
                    <input type="text" name="date_received[]" class="selectDate" autocomplete="off"   style="padding:6px 20px; "
                           readonly data-bv-notempty="true"
                           data-bv-notempty-message="The Date Received is and cannot be empty"  />
                </div>
            </div>
        </div>
    </div>
</div>
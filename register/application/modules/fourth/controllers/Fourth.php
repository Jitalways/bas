<?php

class Fourth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register_fourth', 'model');
    }

    public function index()
    {
        $info = NULL;
        $info_awards = NULL;
        $param = NULL;

        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
            //get info
            $info =$this->model->getRegister($this->uid);
            $info_awards =$this->model->getAwards($this->uid);
        }
        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            $param['register_id'] = $this->uid;

            //update data
            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'));
            if($update_id){
                //upload image
                if (!empty($_FILES['relate_document']['name'])) {
                    $this->load->model('model_upload');
                    $file_name = $this->model_upload->upload_file('relate_document', $this->uid, 'all');
                    if($file_name !== FALSE){
                        $this->model->update_file($update_id, $file_name);
                        $this->model_upload->unlink_file($this->input->post('old_relate_document', TRUE));
                    }
                }
                redirect('fifth');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['info_awards'] = $info_awards;
        $this->data['country_list'] = json_decode(file_get_contents(base_url() . 'themes/default/config/countries.json'));
        $this->data['type_competition_list'] = array('English language competition', 'debate', 'speech', 'General knowledge quiz');
        $this->view('fourth');
    }
}

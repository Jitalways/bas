        <div class="container">
            <div class="signup-content">
                <div class="signup-form" style="margin:0px auto;">
                    <?php
                    if (isset($error)) {
                        echo '<div class="alert alert-danger">เกิดความผิดพลาด! ไม่สามารถบันทึกข้อมูลลงระบบได้</div>';
                    } ?>
                    <form method="POST" action="<?php echo base_url(); ?>third" class="register-form" role="form"  id="register-form3">
                        <input type="hidden" name="education_id" value="<?php echo set_value('education_id', ((!empty($info->education_id)) ? $info->education_id : 0)); ?>">
                        <h2>Application Form ( Step 3. ) </h2>
                        <h4>
                            Educational Background
                        </h4>
                        <br/>
                          <div class="form-group">
                              <label for="institution_junior" class="radio-label">Institution :</label>
                              <input type="text" name="institution_junior" id="institution_junior" value="<?php echo set_value('institution_junior', ((!empty($info->institution_junior)) ? $info->institution_junior : '')); ?>" required />
                          </div>
                          <div class="form-group">
                              <label for="city_junior" class="radio-label">City :</label>
                              <input type="text" name="city_junior" id="city_junior" value="<?php echo set_value('city_junior', ((!empty($info->city_junior)) ? $info->city_junior : '')); ?>" required />
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                                <label for="country_junior" class="radio-label">Country :</label>
                                <div class="form-select">
                                    <select name="country_junior" id="country_junior" required>
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach($country_list as $cvalue) {
                                            echo '<option value="'.$cvalue->name.'" '.set_select('country_junior', $cvalue->name,((!empty($info->country_junior) && $info->country_junior==$cvalue->name) ? TRUE : FALSE)).'>'.$cvalue->name.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="education_system_junior" class="radio-label">Education System :</label>
                                <div class="form-select">
                                    <?php $edu_junior_list = array('Thai', 'Bilingual', 'International'); ?>
                                    <select name="education_system_junior" id="education_system_junior" required>
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach($edu_junior_list as $jvalue) {
                                            echo '<option value="'.$jvalue.'" '.set_select('education_system_junior', $jvalue,((!empty($info->education_system_junior) && $info->education_system_junior==$jvalue) ? TRUE : FALSE)).'>'.$jvalue.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                          </div>
                          <div class="form-row">
                          <div class="form-group">
                              <label for="year_graduation_junior" class="radio-label">Years of Graduation :</label>
                               <div class="form-select">
                                    <select name="year_graduation_junior" id="year_graduation_junior" >
                                        <option value="">-- select one --</option>
                                        <?php 
                                        $pre_y = date('Y') - 100;
                                        for ($x = $pre_y; $x <= date('Y'); $x++) {
                                         echo '<option value="'.$x.'" '.set_select('year_graduation_junior', $x,((!empty($info->year_graduation_junior) && $info->year_graduation_junior==$x) ? TRUE : FALSE)).'>'.$x.'</option>';
										} ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                          </div>
                          <div class="form-group">
                                <label for="gpa_junior" class="radio-label">Cumulative GPA or Current GPA :</label>
                                <input type="text" name="gpa_junior" id="gpa_junior" value="<?php echo set_value('gpa_junior', ((!empty($info->gpa_junior)) ? $info->gpa_junior : '')); ?>" required />
                            </div>
                          </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="type_certificate_junior" class="radio-label">Types of Certificates used for this Application :</label>

                              <div class="form-select">
                                  <?php $tcir_junior_list = array('GCE', 'GCSE', 'IGCSE', 'IB', 'GED', 'High school GPA'); ?>
                                  <select name="type_certificate_junior" id="type_certificate_junior" required>
                                      <option value="">-- select one --</option>
                                      <?php
                                      foreach($tcir_junior_list as $tcvalue) {
                                          echo '<option value="'.$tcvalue.'" '.set_select('type_certificate_junior', $tcvalue,((!empty($info->type_certificate_junior) && $info->type_certificate_junior==$tcvalue) ? TRUE : FALSE)).'>'.$tcvalue.'</option>';
                                      }
                                      ?>
                                  </select>
                                  <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                              </div>
                           </div>
                          <div class="form-group">
                          </div>
                        </div>
                        <hr/>
                        <p>
                          <h2>
                              High School
                          </h2>
                        </p>
                        <div class="form-group">
                              <label for="institution_high" class="radio-label">Institution :</label>
                              <input type="text" name="institution_high" id="institution_high" value="<?php echo set_value('institution_high', ((!empty($info->institution_high)) ? $info->institution_high : '')); ?>" required />
                          </div>
                          <div class="form-group">
                              <label for="city_high" class="radio-label">City :</label>
                              <input type="text" name="city_high" id="city_high" value="<?php echo set_value('city_high', ((!empty($info->city_high)) ? $info->city_high : '')); ?>" required />
                          </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="country_high" class="radio-label">Country :</label>
                              <div class="form-select">
                                    <select name="country_high" id="country_high">
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach($country_list as $cvalue) {
                                            echo '<option value="'.$cvalue->name.'" '.set_select('country_high', $cvalue->name,((!empty($info->country_high) && $info->country_high==$cvalue->name) ? TRUE : FALSE)).'>'.$cvalue->name.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                          </div>
                          <div class="form-group">
                              <label for="education_system_high" class="radio-label">Education System :</label>
                              <div class="form-select">
                                  <?php $edu_high_list = array('Thai', 'Bilingual', 'International'); ?>
                                  <select name="education_system_high" id="education_system_high">
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach($edu_high_list as $hvalue) {
                                            echo '<option value="'.$hvalue.'" '.set_select('education_system_high', $hvalue,((!empty($info->education_system_high) && $info->education_system_high==$hvalue) ? TRUE : FALSE)).'>'.$hvalue.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="form-group">
                              <label for="type_certificate_high" class="radio-label">Types of Certificates used for this Application :</label>
                              <div class="form-select">
                                  <?php $tcir_high_list = array('GCE', 'GCSE', 'IGCSE', 'IB', 'GED', 'High school GPA'); ?>
                                  <select name="type_certificate_high" id="type_certificate_high" required>
                                      <option value="">-- select one --</option>
                                      <?php
                                      foreach($tcir_high_list as $tcvalue) {
                                          echo '<option value="'.$tcvalue.'" '.set_select('type_certificate_high', $tcvalue,((!empty($info->type_certificate_high) && $info->type_certificate_high==$tcvalue) ? TRUE : FALSE)).'>'.$tcvalue.'</option>';
                                      }
                                      ?>
                                  </select>
                                  <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                              </div>
                          </div>
                          <div class="form-group">
                          </div>
                        </div>
                        <div class="form-submit">
                            <input type="button" value="Back" class="submit btn btn-default" id="reset" />
                            <input type="submit" value="Submit Form" class="submit btn btn-primary"  />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                var cardtype_text = 'The card_no is not valid'
                $('#register-form3').bootstrapValidator(
                    {
                        message: 'This value is not valid',
                        fields: {
                            'institution_junior':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Institution is required and cannot be empty'
                                    }
                                }
                            },
                            'city_junior': {
                                validators: {
                                    notEmpty:{
                                        message: 'The City is required and cannot be empty'
                                    }
                                }
                            },
                            'country_junior':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Country is required and cannot be empty'
                                    }
                                }
                            },
                            'education_system_junior': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Education System is required and cannot be empty'
                                    }
                                }
                            },
                            'year_graduation_junior': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Year of Graduation is required and cannot be empty'
                                    }
                                }
                            },
                            'gpa_junior': {
                                validators: {
                                    notEmpty: {
                                        message: 'The GPA is required and cannot be empty'
                                    },
                                    callback: {
                                        message: 'GPA is not yet valid.',
                                        callback: function (value, validator, $field) {
                                                var format = /^([0-9]{1,2})(\.[0-9]{1,2})?$/;
                                                return format.test(value) == true
                                            
                                        }
                                    },
                                }
                            },
                            'type_certificate_junior': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Type Certificate is required and cannot be empty'
                                    }
                                }
                            },
                            'institution_high': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Institution is required and cannot be empty'
                                    }
                                }
                            },
                            'city_high': {
                                validators: {
                                    notEmpty: {
                                        message: 'The City is required and cannot be empty'
                                    }
                                }
                            },
                            'country_high': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Country is required and cannot be empty'
                                    }
                                }
                            },
                            'education_system_high': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Education System is required and cannot be empty'
                                    }
                                }
                            },
                            'type_certificate_high': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Type Certificate is required and cannot be empty'
                                    }
                                }
                            }
                        }
                    })
                })
        </script>

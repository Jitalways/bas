<?php

class Third extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register_third', 'model');
    }

    public function index()
    {
        $info = NULL;
        $param = NULL;

        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
            //get info
            $info =$this->model->getRegister($this->uid);
        }
        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            $param['register_id'] = $this->uid;

            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'));
            if($update_id){
                redirect('fourth');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['country_list'] = json_decode(file_get_contents(base_url() . 'themes/default/config/countries.json'));
        $this->view('third');
    }
}

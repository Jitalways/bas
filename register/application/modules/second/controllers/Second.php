<?php

class Second extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register', 'model');
    }

    public function index()
    {
        $info = NULL;
        $param = NULL;

        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
          //get info
          $info =$this->model->getRegister($this->uid);
          if (empty($info)) {
            redirect('first');
          }
        }
        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();

            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'), 'second');
            if($update_id){
                redirect('third');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['area_code_list'] = json_decode(file_get_contents(base_url() . 'themes/default/config/CountryCodes.json'));
        $this->view('second');
    }

}

        <div class="container">
            <div class="signup-content">
                <div class="signup-form" style="margin:0px auto;">
                    <?php
                    if (isset($error)) {
                        echo '<div class="alert alert-danger">เกิดความผิดพลาด! ไม่สามารถบันทึกข้อมูลลงระบบได้</div>';
                    } ?>
                    <form method="POST" action="fifth" class="register-form" role="form"  id="register-form5" enctype="multipart/form-data">
                        <input type="hidden" name="skill_id" value="<?php echo set_value('skill_id', ((!empty($info->skill_id)) ? $info->skill_id : 0)); ?>">
                        <h2>Application Form ( Step 5. )</h2>
                        <h4>
                            English language test result
                        </h4>
                        <br/>
                          <div class="form-row">
                            <div class="form-group">
                                <label for="eng_type_test" class="radio-label">Type of test :</label>
                                <div class="form-select">
                                    <select name="eng_type_test" id="eng_type_test">
                                        <option value="">-- select one --</option>
                                        <?php
                                        foreach($eng_type_test_list as $evalue) {
                                            echo '<option value="'.$evalue.'" '.set_select('eng_type_test', $evalue,((!empty($info->eng_type_test) && $info->eng_type_test==$evalue) ? TRUE : FALSE)).'>'.$evalue.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="select-icon"><i class="zmdi zmdi-chevron-down"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                            </div>
                          </div>
                          <div class="form-row">
                          <div class="form-group">
                              <label for="test_scores" class="radio-label">Test scores results :</label>
                              <input type="text" name="test_scores" id="test_scores" value="<?php echo set_value('test_scores', ((!empty($info->test_scores)) ? $info->test_scores : '')); ?>" required />
                          </div>
                          <div class="form-group">
                            </div>
                          </div>
                          <div class="form-row">
                          <div class="form-group">
                                <label for="eng_date_taken" class="radio-label">Date Taken :</label>
                                <input type="text" name="eng_date_taken" id="eng_date_taken" autocomplete="off"  readonly value="<?php echo set_value('eng_date_taken', ((!empty($info->eng_date_taken)) ? $info->eng_date_taken : '')); ?>" required />
                            </div>
                            <div class="form-group">
                            </div>
                          </div>
                        
                        <p>
                          <h2>
                          Statement of Purpose
                          </h2>
                        </p>
                        <div class="form-group">
                              <label for="zipcode" class="radio-label">Give reasons why you plan to study British & American Studies at Thammasat University and outline your intended career path upon graduating (Do not exceed 500 words). :</label>
                              <textarea name="statement_purpose" cols="40" rows="5" maxlength="500"><?php echo set_value('statement_purpose', ((!empty($info->statement_purpose)) ? $info->statement_purpose : '')); ?></textarea>
                          </div>

                          <hr/>
                          <p>
                            <h2 style="margin-bottom: 0px;">
                              Please upload related documents
                            </h2>
                            <label> Note: For portfolio round, applicants must provide the following evidence :</label>
                          </p>
<br/>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="statement_purpose" class="radio-label">1)	Statement of purpose</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="statement" class="fileCheck" >
                                <?php if (!empty($info->statement)) {?><i class="glyphicon glyphicon-file"><?php echo $info->statement; ?></i><?php }?>
                                <input type="hidden" name="old_statement" value="<?php echo set_value('statement', ((!empty($info->statement)) ? $info->statement : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="evidence_high_school" class="radio-label">2)	Evidence of high school completion or a student status confirmation letter</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="evidence_high_school" class="fileCheck">
                                <?php if (!empty($info->evidence_high_school)) {?><i class="glyphicon glyphicon-file"><?php echo $info->evidence_high_school; ?></i><?php }?>
                                <input type="hidden" name="old_evidence_high_school" value="<?php echo set_value('evidence_high_school', ((!empty($info->evidence_high_school)) ? $info->evidence_high_school : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="transcripts" class="radio-label">3)	Transcripts covering no less than four semesters</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="transcripts" class="fileCheck">
                                <?php if (!empty($info->transcripts)) {?><i class="glyphicon glyphicon-file"><?php echo $info->transcripts; ?></i><?php }?>
                                <input type="hidden" name="old_transcripts" value="<?php echo set_value('transcripts', ((!empty($info->transcripts)) ? $info->transcripts : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="zipcode" class="radio-label">4)	Evidence of English language test results</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="evidence_english" class="fileCheck">
                                <?php if (!empty($info->evidence_english)) {?><i class="glyphicon glyphicon-file"><?php echo $info->evidence_english; ?></i><?php }?>
                                <input type="hidden" name="old_evidence_english" value="<?php echo set_value('evidence_english', ((!empty($info->evidence_english)) ? $info->evidence_english : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="zipcode" class="radio-label">5)	Evidence showing the participation in the study abroad programme</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="evidence_showing" class="fileCheck">
                                <?php if (!empty($info->evidence_showing)) {?><i class="glyphicon glyphicon-file"><?php echo $info->evidence_showing; ?></i><?php }?>
                                <input type="hidden" name="old_evidence_showing" value="<?php echo set_value('evidence_showing', ((!empty($info->evidence_showing)) ? $info->evidence_showing : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="zipcode" class="radio-label">6)	Evidence of a prize at a national or international level</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="evidence_prize" class="fileCheck">
                                <?php if (!empty($info->evidence_prize)) {?><i class="glyphicon glyphicon-file"><?php echo $info->evidence_prize; ?></i><?php }?>
                                <input type="hidden" name="old_evidence_prize" value="<?php echo set_value('evidence_prize', ((!empty($info->evidence_prize)) ? $info->evidence_prize : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="zipcode" class="radio-label">7)	Photo</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="photo" class="fileCheck">
                                <?php if (!empty($info->photo)) {?><i class="glyphicon glyphicon-file"><?php echo $info->photo; ?></i><?php }?>
                                <input type="hidden" name="old_photo" value="<?php echo set_value('photo', ((!empty($info->photo)) ? $info->photo : '')); ?>" />
                            </div>
                          </div>
                          <div class="form-row">
                            <div class="form-group">
                              <label for="zipcode" class="radio-label">8)	Others</label>
                            </div>
                            <div class="form-group">
                              <input type="file" name="others" class="fileCheck">
                                <?php if (!empty($info->others)) {?><i class="glyphicon glyphicon-file"><?php echo $info->others; ?></i><?php }?>
                                <input type="hidden" name="old_others" value="<?php echo set_value('others', ((!empty($info->others)) ? $info->others : '')); ?>" />
                            </div>
                          </div>
                        <div class="form-submit">
                        <input type="button" value="Back" class="submit btn btn-default" id="reset" />
                            <input type="submit" value="Submit Form" class="submit btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#eng_date_taken').datepicker({
                    viewMode:0,
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });

                var cardtype_text = 'The card_no is not valid'

               // var extension_file = "pdf, doc, docx";
               // var type_file = "application/pdf, application/msword, application/vnd.ms-office, application/vnd.openxmlformats-officedocument.wordprocessingml.document";
               // var extension_img ="jpeg, jpg, png, x-png";
               // var type_img ="image/jpeg, image/pjpeg, image/png, image/x-png";

                $('#register-form5').bootstrapValidator(
                    {
                        message: 'This value is not valid',
                        fields: {
                            'eng_type_test':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Type of Test is required and cannot be empty'
                                    }
                                }
                            },
                            'test_scores': {
                                validators: {
                                    notEmpty:{
                                        message: 'The Test Scores Result  is required and cannot be empty'
                                    },
                                    integer: {
                                        message: 'The value is not an integer'
                                    }
                                }
                            },
                            'eng_date_taken':{
                                validators: {
                                    notEmpty: {
                                        message: 'The Date Taken is required and cannot be empty'
                                    }
                                }
                            },
                            'statement_purpose': {
                                validators: {
                                    notEmpty: {
                                        message: 'The Statment Purpose is required and cannot be empty'
                                    },
                                    stringLength: {
                                        enabled: true,
                                        min: 1,
                                        max: 500,
                                        message: 'The Statment Purpose must be less than 500 characters long'
                                    }
                                }
                            },
                            'statement': {
                                <?php if (!empty($info->statement)) echo 'excluded: true,';?>
                                validators: {
                                    notEmpty: {
                                        message: 'Please upload document'
                                    },
                                    file: {
                                        extension: 'jpeg,png,doc,docx,pdf',
                                        type: 'image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                        maxSize: 2097152,
                                        message: 'The selected file is not valid'
                                    }
                                }
                            },
                            'evidence_high_school': {
                                <?php if (!empty($info->evidence_high_school)) echo 'excluded: true,';?>
                                validators: {
                                    notEmpty: {
                                        message: 'Please upload document'
                                    },
                                    file: {
                                        extension: 'jpeg,png,doc,docx,pdf',
                                        type: 'image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                        maxSize: 2097152,
                                        message: 'The selected file is not valid'
                                    }
                                }
                            },
                            'transcripts': {
                                <?php if (!empty($info->transcripts)) echo 'excluded: true,';?>
                                validators: {
                                    notEmpty: {
                                        message: 'Please upload document'
                                    },
                                    file: {
                                        extension: 'jpeg,png,doc,docx,pdf',
                                        type: 'image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                        maxSize: 2097152,
                                        message: 'The selected file is not valid'
                                    }
                                }
                            },
                            'evidence_english': {
                                <?php if (!empty($info->evidence_english)) echo 'excluded: true,';?>
                                validators: {
                                    notEmpty: {
                                        message: 'Please upload document'
                                    }
                                }
                            },
                            'photo': {
                                <?php if (!empty($info->photo)) echo 'excluded: true,';?>
                                validators: {
                                    notEmpty: {
                                        message: 'Please upload document'
                                    },
                                    file: {
                                        extension: 'jpeg,png',
                                        type: 'image/jpeg,image/png',
                                        maxSize: 2097152,
                                        message: 'The selected file is not valid'
                                    }
                                }
                            }
                        }
                    })
            })
        </script>

<?php

class Fifth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_register_fifth', 'model');
    }

    public function index()
    {
        $info = NULL;
        $param = NULL;

        /* ==================== Query start data ======================= */
        if (!empty($this->uid)) {
           //get info
            $info =$this->model->getRegister($this->uid);
        }
        /* ==================== Submit form data ======================= */
        if ($this->input->post()) {
            $param = $this->input->post();
            $param['register_id'] = $this->uid;

            //update data
            $update_id = $this->model->updateRegister($param , ((!empty($info)) ? 'update' : 'insert'), 'second');
            if($update_id){
                //upload image
                $this->load->model('model_upload');
                $upload_list = array('statement', 'evidence_high_school', 'transcripts', 'evidence_english', 'evidence_showing', 'evidence_prize', 'photo', 'others');
                foreach($upload_list as $field) {
                    if (!empty($_FILES[$field]['name'])) {
                        $file_name = $this->model_upload->upload_file($field, $this->uid, 'all');
                        if ($file_name !== FALSE) {
                            $this->model->update_file($update_id, $field, $file_name);
                            $this->model_upload->unlink_file($this->input->post('old_'.$field, TRUE));
                        }
                    }
                }
                //send mail
                $this->send_mail();

                redirect('thankyou');
            }else{
                $this->data['error'] = TRUE;
            }
        }

        /* ==================== Render View ======================= */
        $this->data['info'] = $info;
        $this->data['eng_type_test_list'] =  array('IELTS', 'TU-GET', 'SAT', 'TOEFL', 'iBT');
        $this->view('fifth');
    }

    private function send_mail(){
        $this->load->model('Model_register', 'model_first');
        $register_info = $this->model_first->getRegister($this->uid);
        if (!empty($register_info)) {

            $this->load->library('email');

            $this->email->from('admin@bas.com', 'BAS');
            $this->email->to($register_info->email);
           // $this->email->cc('another@another-example.com');
           // $this->email->bcc('them@their-example.com');
            $this->email->subject('Register BAS Complete');
            $this->email->message('Register BAS Complete. Thank you.');
            $this->email->send();
        }
    }


}

<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * js_asset()
 *
 * @param	string $filename
 * @param	bool   $script default TRUE
 * @param	string $dir default "assets/js" in this site
 * @return	mixed (echo js script tag or return path file js)
 */
function js_asset($filename, $script = TRUE, $dir = 'assets/js') {
    $path = base_url().trim($dir , '/').'/'.$filename;
    return ($script === TRUE) ?  '<script type="text/javascript" src="'.$path.'"></script>' : $path;
}

/**
 * js_asset_theme()
 *
 * @param	string $filename
 * @param	bool   $script default TRUE
 * @param	string $theme default "default" in theme this site
 * @return	mixed (echo js script tag or return path file js)
 */
function js_asset_theme($filename, $script = TRUE, $theme = 'default') {
    return js_asset($filename , $script , 'themes/'.$theme.'/js')."\n";
}

function js_asset_url($url) {
    return '<script type="text/javascript" src="'.$url.'"></script>';
}


/**
 * css_asset()
 *
 * @param	string $filename
 * @param	bool   $script default TRUE
 * @param	string $dir default "assets/css" in this site
 * @return	mixed (echo js script tag or return path file css)
 */
function css_asset($filename, $script = TRUE, $dir = 'assets/css') {
    $path = base_url().trim($dir , '/').'/'.$filename;
    return ($script === TRUE) ?  '<link type="text/css" rel="stylesheet" href="'.$path.'"  media="all">' : $path;
}

/**
 * css_asset_theme()
 *
 * @param	string $filename
 * @param	bool   $script default TRUE
 * @param	string $theme default "default" in theme this site
 * @return	mixed (echo js script tag or return path file css)
 */
function css_asset_theme($filename, $script = TRUE, $theme = 'default') {
    return css_asset($filename , $script , 'themes/'.$theme.'/css');
}


function css_asset_url($url) {
    return '<link type="text/css" rel="stylesheet" href="'.$url.'">';
}


/**
 * img_asset()
 *
 * @param	string $filename
 * @param	string $dir default "assets/images" in this site
 * @return	mixed return path file image
 */
function img_asset($filename, $dir = 'assets/images') {
    return base_url().trim($dir , '/').'/'.$filename;
}

/**
 * img_asset_theme()
 *
 * @param	string $filename
 * @param	string $theme default "default" in theme this site
 * @return	mixed return path file image
 */
function img_asset_theme($filename, $theme = 'default') {
    return img_asset($filename , 'themes/'.$theme.'/images');
}


function slugify($text, $encode = TRUE) {
    $text = preg_replace('!\s+!', '-', $text);
    $text = str_replace([' – ', '–', ' - ','–','/', '(',')','"','\'','.',',','&','?'],['-','-','-','-','','','','','','','',''], $text);
    $text = mb_strtolower($text);
    return (($encode === TRUE) ? urlencode($text) : $text);
}


function _month($m, $lang = 'th') {
    $m = (int) $m;
    $month_name = array(
        'th' => array('-', 'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'),
        'en' => array('-', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')
        );
    return $month_name[$lang][$m];
}

function show_date($str_date = 0) {
    if (is_numeric($str_date) || strlen($str_date) != 19) {
        $str_date = date('Y-m-d H:i:s', strtotime($str_date));
    }

    $now = date('Y-m-d');
    $yesterday = date("Y-m-d", strtotime("- 1 day"));
    list($date, $time) = explode(" ", $str_date);

    if($date == $now)  {
        $start = date_create($str_date);
        $end = date_create(date('Y-m-d H:i:s'));
        $diff = date_diff($end, $start);
        if ($diff->h > 0) {
            return $diff->h.' ชั่วโมง';
        } else {
            return $diff->i.' นาที';
        }
    } else if ($date == $yesterday) {
        return 'เมื่อวานนี้ '.substr($time,0,5).' น.';
    } else {
        list($y, $m, $d) = explode("-", $date);
        return $d.' '._month($m).' '.($y+543);
    }
}


function title_tag($text, $cut = FALSE) {
    $text = str_replace(array('"', "'", '>', '<'), array('', '', '', ''), trim($text));
    if ($cut !== FALSE) {
        $len = (int) $cut;
        $text = mb_substr($text, 0, $len);
    }
    return $text;
}


function active_menu($menu, $page = 'home') {
    if ($menu == $page) {
        echo 'class="active"';
    }
}


/*function replaceAds($matches) {
    static $count = 0;
    $ret = $matches[1];
    if (++$count == 2) {
        $ret .= "\n".'<div id="ad-content-read" class="ad-content-read"></div>';
    }
    return $ret;
}*/


function replaceAds($text, $paragraph_no = 2) {
    $ads_box  = '<div id="ad-content-read" class="ad-content-read"></div>'."\n";
    $total = (int) preg_match_all('#(<p(.*?)</p>)#s', $text, $matches);
    if ($total > $paragraph_no) {
        $regex = "/(?s)((?:<p(.*?)<\\/p>\\s*){".$paragraph_no."})/";
        $result = preg_replace($regex, "$1".$ads_box, $text, 1);
    } else {
        $result = $text.$ads_box;
    }
    return $result;
}


function replaceContentDetail($html, $ads = FALSE) {
    $text = preg_replace('/\[activity\](.*?)\[\/activity\]/', '<div class="activity-widget activity-widget-$1" data-activity="$1"></div>', $html);
    if ($ads === TRUE) {
        $text = replaceAds($text);
    }

    return $text;
}



function replaceToWeb($url) {
    $text = str_replace('m.', 'www.', $url);
    return $text;
}


function clearFormatApp($html) {
    $text = preg_replace('/\[activity\](.*?)\[\/activity\]/', '', $html);
    $text = str_replace(array('<center>','</center>'), array('<p style="text-align:center">', '</p>'), $text);

    return $text;
}


function get_ip() {
    $ip_address = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ip_address = getenv('HTTP_CLIENT_IP');
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip_address = getenv('HTTP_X_FORWARDED_FOR');
    } else if(getenv('HTTP_X_FORWARDED')) {
        $ip_address = getenv('HTTP_X_FORWARDED');
    } else if(getenv('HTTP_FORWARDED_FOR')) {
        $ip_address = getenv('HTTP_FORWARDED_FOR');
    } else if(getenv('HTTP_FORWARDED')) {
        $ip_address = getenv('HTTP_FORWARDED');
    } else if(getenv('REMOTE_ADDR')) {
        $ip_address = getenv('REMOTE_ADDR');
    } else {
        $ip_address = 'UNKNOWN';
    }
    return $ip_address;
}


function log_counter_view($content_id){
    //opens countlog.txt to read the number of hits
    $datei = @fopen(VIEWPATH.'count_view/'.$content_id.".txt","r");
    $count = @fgets($datei,1000);
    @fclose($datei);
    $count=$count + 1 ;
    //echo "$count" ;
    //echo " hits" ;
    //echo "\n" ;

    // opens countlog.txt to change new hit number
    $datei = @fopen(VIEWPATH.'count_view/'.$content_id.".txt","w");
    fwrite($datei, $count);
    @fclose($datei);

}

function log_counter_view_session($content_id, $session_id){

    $file =VIEWPATH.'count_view/'.$content_id.".txt";

    /*if( strpos(@file_get_contents($file),$session_id) !== false) {
        // do stuff
    }else{*/
        $datei = @fopen($file,"a");
        fwrite($datei, $session_id."  ".date('Y-m-d H:i:s')."\n");
        @fclose($datei);
    //}

}
<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Controller class */
//require APPPATH."third_party/MX/Controller.php";
class MY_Controller extends CI_Controller
{

    /* Global Variable */
    public $theme = 'default';
    public $layout = 'global';
    public $partials = array('header', 'footer');
    public $data = array();
    public $uid = FALSE;

    public function __construct()
    {
        parent::__construct();

        if (!empty($_COOKIE['_bas_member'])) {
            $this->uid = $_COOKIE['_bas_member'];
        }

        // Default Site Config
        $data = array('theme' => $this->theme);

        // Set Default View
        $this->data = $data;
        unset($data);
    }

    public function view($view_file, $data = NULL, $layout = NULL, $template = NULL)
    {
        if (empty($data)) {
            $data = $this->data;
        }
        if (empty($template)) {
            $template = $this->theme;
            $this->template->_template = $template;
        }
        if (empty($layout)) {
            $layout = $this->layout;
        }
        if (!empty($this->partials)) {
            foreach ($this->partials as $p) {
                $this->template->set_partial($p);
            }
        }
        $this->template->set_layout($layout, $view_file, $data);
        $this->template->build();
    }


    public function js_theme($filename, $script = TRUE)
    {
        return js_asset_theme($filename, $script, $this->theme);
    }


    public function css_theme($filename, $script = TRUE)
    {
        return css_asset_theme($filename, $script, $this->theme);
    }


    public function _setCookie($data) {
        if (is_array($data)) {
            foreach($data as $key => $val) {
                setcookie(BAS_COOKIE_PREFIX.$key, $val, time()+BAS_COOKIE_AGE, '/', BAS_COOKIE_DOMAIN);
            }
        }
    }

    public function _removeCookie($data = array('member')) {
        if (is_array($data)) {
            foreach($data as $key) {
                setcookie(BAS_COOKIE_PREFIX.$key, '', time(), '/', SMB_COOKIE_DOMAIN);
            }
        } else {
            setcookie(BAS_COOKIE_PREFIX.$data, '', time()+BAS_COOKIE_AGE, '/', BAS_COOKIE_DOMAIN);
        }
    }
}
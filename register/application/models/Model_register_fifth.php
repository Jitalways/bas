<?php

class Model_register_fifth extends MY_Model {

    public $error = [];
    private $_tb_register = ['register_id', 'eng_type_test', 'test_scores', 'eng_date_taken', 'statement_purpose', 'statement', 'evidence_high_school', 'transcripts', 'evidence_english', 'evidence_showing', 'evidence_prize', 'photo', 'others'];

     public function __construct() {
        parent::__construct();
    }

    public function getRegister($id) {
        $this->db->where('register_id', $id);
        $this->db->limit(1);
        $sql = $this->db->get('register_skill_related');
        return $sql->row();
    }

    public function updateRegister($params, $type = 'update')
    {
        $act = FALSE;
        $skill_id = $params['skill_id'];
        $register_id = $params['register_id'];

        $update = $this->filterTBRegister($params, $this->_tb_register);
        unset($params);

        if (!empty($skill_id)) {
            // =================================== UPDATE ================================
            $this->db->where('skill_id', $skill_id)->update('register_skill_related' , $update['register']);
            $act = $skill_id;
            //update status
            $this->db->where('register_id', $register_id)->update('register' , array('active'=>1, 'update_date'=>date('Y-m-d H:i:s')));

        } else if ($type == 'insert') {
            // =================================== INSERT ================================
            if (!empty($update['register'])) {
                $this->db->insert('register_skill_related' , $update['register']);
                $act = $this->db->insert_id();

                //update status
                $this->db->where('register_id', $register_id)->update('register' , array('active'=>1, 'update_date'=>date('Y-m-d H:i:s')));
            }
        }
        return $act;
    }

    private function filterTBRegister($params, $table) {
        $update = array(
            'register' => [],
        );
        $fill_content = array_fill_keys($table, 0);
        $update['register'] = array_intersect_key($params, $fill_content);
        return $update;
    }

    public function update_file($id, $field, $file)
    {
        $this->db->where('skill_id', $id)->update('register_skill_related', [$field =>$file]);
    }
}
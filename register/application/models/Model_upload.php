<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_upload extends CI_Model {

    public $root_folder = 'upload/';

    public function __construct() {
        parent::__construct();
    }

    public function unlink_file($file){
        if (file_exists($this->root_folder.$file)) {
            @unlink($this->root_folder.$file);
        }
    }

    public function directory_exists($dir){
        if (! is_dir($dir)) {
            mkdir($dir, 0755, TRUE);
        }
    }

    public function upload_file($file_name, $id, $type_file='all'){

        $this->load->library('upload');

        $sub_folder = $id.'/';
        $upload_path = $this->root_folder.$sub_folder;
        $this->directory_exists($upload_path);

        //set config file
        $config['upload_path'] = $upload_path;
        $config['file_name'] = md5($id.time());
        if($type_file=='image') {
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size'] = 1024;
        }elseif($type_file=='file'){
            $config['allowed_types'] = 'pdf|docx|doc';
            $config['max_size'] = 1024;
       }else{
            $config['allowed_types'] = 'pdf|docx|doc|jpeg|jpg|png';
            $config['max_size'] = 1024;
       }
        $this->upload->initialize($config, TRUE);

        if ( ! $this->upload->do_upload($file_name)) {
            $this->error = strip_tags($this->upload->display_errors().' (Image : '.$file_name.')');
            return FALSE;
        } else {
            //upload file
            $image_data = $this->upload->data();

            //chmod file
            chmod($upload_path.$image_data['file_name'], 0644);

            //return path file
            return $sub_folder.$image_data['file_name'];
        }
    }

} // End of class